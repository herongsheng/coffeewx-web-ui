import request from '@/utils/request'

export function loginByUsername(username, password) {
  const data = {
    username,
    password
  }
  return request({
    url: '/api/auth/login',
    method: 'post',
    data
  })
}

export function logout(token) {
  const data = {
    token
  }
  return request({
    url: '/api/auth/logout',
    method: 'post',
    params: data
  })
}

export function getUserInfo() {
  var data = {
  }
  return request({
    url: '/api/user/getUserInfo',
    method: 'post',
    data
  })
}

